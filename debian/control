Source: lv2
Section: sound
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Alessio Treglia <alessio@debian.org>,
 Jaromír Mikeš <mira.mikes@seznam.cz>,
 Dennis Braun <d_braun@kabelmail.de>
Build-Depends:
 debhelper-compat (= 13),
 libcairo2-dev,
 libsndfile1-dev,
 meson,
 ninja-build,
 pkgconf
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/multimedia-team/lv2.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/lv2
Homepage: https://lv2plug.in/
Rules-Requires-Root: no

Package: lv2-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends}
Description: LV2 audio plugin specification
 LV2 is a simple but extensible successor of LADSPA plugins,
 intended to address the limitations of LADSPA which many
 applications have outgrown.
 .
 This package contains the LV2 audio plugin specification,
 with all the official extension packages, as well as example
 plugins, and additional data.
 .
 Implementations are encouraged to abandon the “copy paste
 headers” practice and depend on this package instead.

Package: lv2-examples
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Provides:
 lv2-plugin
Description: LV2 audio plugin specification (example plugins)
 LV2 is a simple but extensible successor of LADSPA plugins,
 intended to address the limitations of LADSPA which many
 applications have outgrown.
 .
 This package contains some LV2 example plugins.
